

//--------------------------

var toast = function(data){
	$.oc.flashMsg({
		    'text': data.success ? 'Item added' : 'failed',
		    'class': data.success ? 'success' : 'error',
		    'interval': 3
	});
}

var refresh = function(){
	setTimeout(window.location.reload(), 5000);
}

var addBobotItem = function(event){
	event.preventDefault();

	$('#formAddBobotItem').request('onAddBobotItem', {
	    success: function(data){
			toast(data);	
	    	$('#add-bobot-item').modal('hide');
	    	refresh();
	    }
	})


}


var initTable = function(){



	$('#addBobotItemBtn').on('click', function(event){
		event.preventDefault();
		$('#modalTitle').text('Tambah Bobot Item');
		$('#add-bobot-item').modal('show');

	});

	$('#addKriteriaBtn').on('click', function(event){
		event.preventDefault();
		$('#modalTitle').text('Tambah Kriteria Item Mapping');
		$('#add-kriteria-item').modal('show');

	});


	$('.btn-edit-bobot').on('click', function(event){
		event.preventDefault();

		$('#modalTitle').text('Edit Bobot Item');
		var itemId = $(this).attr('data-record-id');
		
		$.request('onGetBobotItem',{
			data: { id: itemId},
			success: function(data){
				$('#BobotItem_label').val(data.data.label);
				$('#BobotItem_nilai_min').val(data.data.nilai_min);
				$('#BobotItem_nilai_max').val(data.data.nilai_max);
				$('#BobotItem_bobot').val(data.data.bobot);
		    	$('#add-bobot-item').modal('show');
			}

		});
	})
};

var initSubkriteria = function(){
	$('#Form-field-Subkriteria-unit').on('change', function(event){
		event.preventDefault();
		var value = $(this).val();
		
		$.request('onGetBobotItem', {
			data: {bobot_id:value},
			success: function(response){
			    //var origOptions = $('#SubkriteriaStandar').data('select2').options.options;
			    //console.log(origOptions);
			    //$('#SubkriteriaStandar').select2('destroy').empty().select2($.extend(origOptions, { data: response.data }));
			    $('#SubkriteriaStandar').select2('destroy');
			    $('#SubkriteriaStandar').empty();
			    $.each(response.data,function(key,val){
			    	$('#SubkriteriaStandar').append('<option value="'+val.id+'">'+val.label+'</option>');
			    });

				$('#SubkriteriaStandar').select2({data: response.data});

			}
		});
	})
}

$(document).ready(function(){
	initTable();
	initSubkriteria();

	$('.table.data').dataTable( {
	  "autoWidth": false
	});

	$('.pm_field').on('change', function(event){
		event.preventDefault();

		$('#pm-setting').request('onUpdatePMSettings',{
			success: function(data){
				$.oc.flashMsg({
				    'text': 'Record saved.',
				    'class': 'success',
				    'interval': 3
				});
			},
			error: function(data){
				$.oc.flashMsg({
				    'text': 'Record failed to save.',
				    'class': 'error',
				    'interval': 3
				});	
			}

		});
	})

})