<?php namespace Prudent\SPK;

use Backend;
use System\Classes\PluginBase;
use Prudent\SPK\Models\Siswa;
use Prudent\SPK\Models\Kriteria;
use Prudent\SPK\Models\Nilai;
use Prudent\SPK\Models\BobotItem;

/**
 * SPK Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'SPK',
            'description' => 'No description provided yet...',
            'author'      => 'Prudent',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Prudent\SPK\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'prudent.spk.some_permission' => [
                'tab' => 'SPK',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'spk' => [
                'label'       => 'Master data',
                'url'         => Backend::url('prudent/spk/siswa'),
                'icon'        => 'icon-database',
                'permissions' => ['prudent.spk.*'],
                'order'       => 500,

                'sideMenu'    => [
                    'siswa' => [
                        'label'       => 'Siswa/i',
                        'url'         => Backend::url('prudent/spk/siswa'),
                        'icon'        => 'icon-child',
                        'permissions' => ['prudent.spk.*'],
                        'order'       => 500
                    ],
                    'kelas' => [
                        'label'       => 'Kelas',
                        'url'         => Backend::url('prudent/spk/kelas'),
                        'icon'        => 'icon-graduation-cap',
                        'permissions' => ['prudent.spk.*'],
                        'order'       => 500
                    ],


                    'nilai' => [
                        'label'       => 'Klasifikasi',
                        'url'         => Backend::url('prudent/spk/nilai'),
                        'icon'        => 'icon-tag',
                        'permissions' => ['prudent.spk.*'],
                        'order'       => 500
                    ],

                    'kriteria' => [
                        'label'       => 'Kriteria',
                        'url'         => Backend::url('prudent/spk/kriteria'),
                        'icon'        => 'icon-th-large',
                        'permissions' => ['prudent.spk.*'],
                        'order'       => 500
                    ],

                    'subkriteria' => [
                        'label'       => 'Subkriteria',
                        'url'         => Backend::url('prudent/spk/subkriteria'),
                        'icon'        => 'icon-th',
                        'permissions' => ['prudent.spk.*'],
                        'order'       => 500
                    ],

                    'bobot' => [
                        'label'       => 'Bobot',
                        'url'         => Backend::url('prudent/spk/bobot'),
                        'icon'        => 'icon-stop',
                        'permissions' => ['prudent.spk.*'],
                        'order'       => 500
                    ]
                ]
            ],

            'process' => [
                'label'       => 'Proses Data',
                'url'         => Backend::url('prudent/spk/metodesaw'),
                'icon'        => 'icon-gear',
                'permissions' => ['prudent.spk.*'],
                'order'       => 500,

                'sideMenu'    => [

                    'saw' => [
                        'label'       => 'SAW',
                        'url'         => Backend::url('prudent/spk/metodesaw'),
                        'icon'        => 'icon-sticky-note',
                        'permissions' => ['prudent.spk.*'],
                        'order'       => 500
                    ],
                    'pm' => [
                        'label'       => 'PM',
                        'url'         => Backend::url('prudent/spk/metodepm'),
                        'icon'        => 'icon-sticky-note',
                        'permissions' => ['prudent.spk.*'],
                        'order'       => 500
                    ]
                ]
            ],
        ];
    }



    public function registerListColumnTypes()
    {
        return [
            'status' => function($value) {
                $retval = ''; 
                switch ($value) {
                    case 1:
                        $retval = 'Aktif';
                        break;
                    case 2:
                        $retval = 'NonAktif';
                        break;
                    default:
                        $retval = '';
                        break;
                }; 
                return $retval;
            },


            'nilai' => [$this, 'getNilaiForSiswa'],
            'standar' => [$this, 'getStandarKriteria']
        ];
    }

    public function getNilaiForSiswa($value, $column, $record)
    {

        if(!empty($record)){
            $siswa = Siswa::find($record->id);
            $kriteria = Kriteria::find($column->columnName);

            $nilai = Nilai::where('siswa_id',$siswa->id)
                ->where('kriteria_id',$kriteria->id)
                ->first();
            if(!empty($nilai)){
                if(!empty($nilai->bobotitem)){
                    return $nilai->bobotitem->label;
                }                
            }else{
                return null;
            }

        }else{
            return null;
        }
        
    }


    public function getStandarKriteria($value, $column, $record)
    {

        if(!empty($record)){
            $kriteria = Kriteria::find($record->id);
            if(!empty($kriteria)){
                if(!empty($kriteria->bobot)){
                    if(!empty($kriteria->bobot->detail)){
                        $temp = $kriteria->bobot->detail->where('bobot',$kriteria->nilai_ideal)->first();
                        return !empty($temp)?$temp->label:null;        
                    }    
                }
            }            
        }
        return null;
        
    }


    public function registerSettings()
    {
        return [
            'location' => [
                'label'       => 'Prudent SPK',
                'description' => 'Manage Pengaturan untuk Prudent SPK.',
                'category'    => 'Prudent SPK',
                'icon'        => 'icon-gear',
                'url'         => Backend::url('acme/user/locations'),
                'order'       => 500,
                'keywords'    => 'geography place placement'
            ]
        ];
    }

}
