<?php namespace Prudent\SPK\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Prudent\SPK\Models\Siswa;
use Prudent\SPK\Models\Kriteria;
use Prudent\SPK\Models\Nilai;
use Prudent\SPK\Models\BobotItem;


/**
 * Metode S A W Back-end Controller
 */
class MetodeSAW extends Controller
{
    public $implement = [];
    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Prudent.SPK', 'process', 'metodesaw');

        $this->addCss("/plugins/prudent/spk/assets/css/jquery.dataTables.min.css");
        $this->addJs("/plugins/prudent/spk/assets/js/jquery.dataTables.min.js");
        $this->addJs("/plugins/prudent/spk/assets/js/prudent.js");
    }

    public function index()
    {
        $this->vars['siswa'] = $siswa = Siswa::get();
        $this->vars['kriteria'] = $kriteria = Kriteria::get();
        $this->vars['nilai'] = $nilai = new Nilai();
        $this->vars['item'] = $nilai = new BobotItem();

    }

}
