<?php namespace Prudent\SPK\Controllers;

use Response;
use BackendMenu;
use Backend\Classes\Controller;
use Prudent\SPK\Models\BobotItem;

/**
 * Subkriteria Back-end Controller
 */
class Subkriteria extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Prudent.SPK', 'spk', 'subkriteria');

    }


    public function update($recordId)
    {
        $this->pageTitle = 'Update subkriteria';


        $this->addCss("/plugins/prudent/spk/assets/css/jquery.dataTables.min.css");
        $this->addJs("/plugins/prudent/spk/assets/js/jquery.dataTables.min.js");
        $this->addJs("/plugins/prudent/spk/assets/js/prudent.js");
        
        try {
            $this->vars['recordId'] = $recordId;
            $this->vars['subkriteria'] = $model = $this->formFindModelObject($recordId);
            
            $listBobotItem =null;
            if(!empty($model)){
                if(!empty($model->unit)){
                    $listBobotItem = BobotItem::where('bobot_id',$model->unit->id)->get();    
                }
                
            }
            $this->vars['standarList'] = $listBobotItem;
            
            $this->initForm($model);
        }
        catch (Exception $ex) {
            $this->controller->handleError($ex);
        }
    }


    public function update_onGetBobotItem($recordId)
    {       
        $response['status']=false;
        $response['data']=array();
        $bobot_id = post('bobot_id');

        try {
            $listBobotItem = BobotItem::where('bobot_id',$bobot_id)->orderBy('id','desc')->get();
            $response['data'] = $listBobotItem;
            return Response::json($response);
        }
        catch (Exception $ex) {
            $this->controller->handleError($ex);
        }

        return Response::json($response);
    }

}
