<?php namespace Prudent\SPK\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Kriteria Back-end Controller
 */
class Kriteria extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Prudent.SPK', 'spk', 'kriteria');
    }


    public function listExtendColumns($list)
    {
        /*$list->addColumns([
            'standar' => [
                'label' => 'Nilai Ideal',
                'type'  => 'standar',
                'sortable' => false
            ],
        ]);*/
    }
}
