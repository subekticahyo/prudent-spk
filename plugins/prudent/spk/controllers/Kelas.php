<?php namespace Prudent\SPK\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Prudent\SPK\Models\Kriteria;
use Prudent\SPK\Models\KelasKriteria;
use Prudent\SPK\Models\Nilai;
use Prudent\SPK\Models\Subkriteria;
use Prudent\SPK\Models\Bobot;
use Prudent\SPK\Models\BobotItem;
use Flash;  

/**
 * Kelas Back-end Controller
 */
class Kelas extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Prudent.SPK', 'spk', 'kelas');

        $this->pageTitle = "Edit Kelas";
    }


    public function update($recordId = null, $context = null)
    {
        $this->addCss("/plugins/prudent/spk/assets/css/jquery.dataTables.min.css");
        $this->addJs("/plugins/prudent/spk/assets/js/jquery.dataTables.min.js");
        $this->addJs("/plugins/prudent/spk/assets/js/prudent.js");


        try {


            
            $this->vars['kriteriamapping'] = KelasKriteria::where('kelas_id',$recordId)->get();
             

            $this->vars['nilai'] = $nilai = new Nilai();
            $this->vars['kriteria'] = $kriteria = Kriteria::get();
            $subkriteria = array();
            foreach ($kriteria as $key => $value) {
                $subkriteria[$value->id] = Subkriteria::where('kriteria_id',$value->id)->get();
            }
            $this->vars['subkriteria'] = $subkriteria;

            $bobotitems=array();
            $bobot = Bobot::get();
            foreach ($bobot as $bkey => $bvalue) {
                $bobotitems[$bvalue->id] = BobotItem::where('bobot_id',$bvalue->id)->get();
            }

            $this->vars['valueOptions'] = $bobotitems;
            $model = $this->formFindModelObject($recordId);
            $this->vars['siswa'] = $model;
            

            $this->vars['bobotItem'] = new BobotItem();

            $this->initForm($model);
        }
        catch (Exception $ex) {
            $this->controller->handleError($ex);
        }
    }


    public function update_onSave($recordId = null, $context = null)
    {
        try {
            $kelas = post('Kelas');
            if(!empty($kelas)){
            
                $kelasObj = $this->formFindModelObject($recordId);
                $kelasObj->nama = $kelas['nama'];
                $kelasObj->jurusan = $kelas['jurusan'];
                $kelasObj->tingkat = $kelas['tingkat'];
                $kelasObj->save();
            }


            $kriteria = post('KelasKriteria');
            if(!empty($kriteria)){
                KelasKriteria::whereKelasId($recordId)->delete();
                
                foreach ($kriteria as $key => $value) {

                    $kriteria = KelasKriteria::whereKelasId($recordId)->whereKriteriaId($value)->first();
                    if(empty($kriteria)){
                        $kriteria = new KelasKriteria(); 
                    }

                    $kriteria->kelas_id = $recordId; 
                    $kriteria->kriteria_id = $value;
                    $kriteria->save();    
                    
                }
            }


        }
        catch (Exception $ex) {
            $this->controller->handleError($ex);
        }

        Flash::success("Saved");
    }
}
