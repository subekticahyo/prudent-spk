<?php namespace Prudent\SPK\Controllers;

use Flash;
use BackendMenu;
use Backend\Classes\Controller;
use Prudent\SPK\Models\Kriteria;
use Prudent\SPK\Models\Nilai;
use Prudent\SPK\Models\Subkriteria;
use Prudent\SPK\Models\Bobot;
use Prudent\SPK\Models\BobotItem;
use Prudent\SPK\Models\Siswa as SiswaModel;
use Prudent\SPK\Models\KelasKriteria;

/**
 * Siswa Back-end Controller
 */
class Siswa extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.ImportExportController',
        'Backend.Behaviors.RelationController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $importExportConfig = 'config_import_export.yaml';
    public $relationConfig = 'config_relation.yaml';


    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Prudent.SPK', 'spk', 'siswa');

        $this->pageTitle = "Edit Siswa";
    }



    public function update($recordId = null, $context = null)
    {
        try {



            $bobot = Bobot::get();
            foreach ($bobot as $bkey => $bvalue) {
                $bobotitems[$bvalue->id] = BobotItem::where('bobot_id',$bvalue->id)->get();
            }

            $this->vars['valueOptions'] = $bobotitems;
            $model = $this->formFindModelObject($recordId);
            $this->vars['siswa'] = $model;

            $this->vars['kriteriamapping'] = new KelasKriteria();
            
            $this->vars['nilai'] = $nilai = new Nilai();
            $this->vars['kriteria'] = $kriteria = Kriteria::get();
            foreach ($kriteria as $key => $value) {
                $subkriteria[$value->id] = Subkriteria::where('prudent_spk_subkriterias.kriteria_id',$value->id)->get();
            }
            $this->vars['subkriteria'] = $subkriteria;

            $this->vars['bobotItem'] = new BobotItem();

            $this->initForm($model);
        }
        catch (Exception $ex) {
            $this->controller->handleError($ex);
        }
    }


    public function update_onSave($recordId = null, $context = null)
    {
        try {
            $nilai = post('Nilai');
            if(!empty($nilai)){
                foreach ($nilai as $key => $value) {

                    $nilai = Nilai::whereSiswaId($recordId)->whereKriteriaId($key)->first();
                    if(empty($nilai)){
                        $nilai = new Nilai();
                    }
                    $nilai->siswa_id = $recordId;
                    $nilai->kriteria_id = $key;
                    $nilai->nilai = $value;
                    $nilai->save();    
                    
                }
            }

            $psiswa = post('Siswa');
            if(!empty($psiswa)){

                $siswa = SiswaModel::whereId($recordId)->first();
                if(!empty($siswa)){

                    $siswa->nis = $psiswa['nis'];
                    $siswa->nisn = $psiswa['nisn'];
                    $siswa->nama = $psiswa['nama'];
                    $siswa->jenis_kelamin = $psiswa['jenis_kelamin'];
                    $siswa->kelas = $psiswa['kelas'];
                    $siswa->save();    
                    
                }
                    
            }


        }
        catch (Exception $ex) {
            $this->controller->handleError($ex);
        }

        Flash::success("Saved");
    }
/*
    public function index(){
        try {
            $this->makeList();
        }
        catch (Exception $ex) {
            $this->controller->handleError($ex);
        }

    }*/
}
