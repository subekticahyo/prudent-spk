<?php namespace Prudent\SPK\Controllers;

use Response;
use BackendMenu;
use Backend\Classes\Controller;
use Prudent\SPK\Models\Siswa;
use Prudent\SPK\Models\Kriteria;
use Prudent\SPK\Models\Nilai;
use Prudent\SPK\Models\Settings;
use Prudent\SPK\Models\Bobot;
use Prudent\SPK\Models\BobotItem;
use Config;

/**
 * Metode P M Back-end Controller
 */
class MetodePM extends Controller
{
    public $implement = [];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Prudent.SPK', 'process', 'metodepm');
        $this->addCss("/plugins/prudent/spk/assets/css/jquery.dataTables.min.css");
        $this->addJs("/plugins/prudent/spk/assets/js/jquery.dataTables.min.js");
        $this->addJs("/plugins/prudent/spk/assets/js/prudent.js");
    }

    public function index()
    {
        $this->vars['pm_core'] = Settings::get('pm_core_factor_value');
        $this->vars['pm_secondary'] = Settings::get('pm_secondary_factor_value');
        $this->vars['pm_bobot_id'] = Settings::get('pm_bobot_id');
        
        $this->vars['bobot'] = $bobot = Bobot::get();
        $this->vars['bobotitem'] = $bobot = BobotItem::get();
        $this->vars['siswa'] = $siswa = Siswa::get();
        $this->vars['kriteria'] = $kriteria = Kriteria::get();
    }

    public function onUpdatePMSettings($recordId=null, $context=null)
    {
        $result['success'] = false;
        $result['data'] = null;

        try {
            $data = post();
            
            if(!empty($data)){
                Settings::set('pm_core_factor_value',$data['pm_core']); 
                Settings::set('pm_secondary_factor_value',$data['pm_secondary']);
                Settings::set('pm_bobot_id',$data['pm_bobot_id']);     
            }

            $result['success'] = true;
            $result['data'] = "ok";
        }
        catch (Exception $ex) {
            $result['success'] = false;
            $result['data'] = $ex;
        }
        return Response::json($result);
    }


}
