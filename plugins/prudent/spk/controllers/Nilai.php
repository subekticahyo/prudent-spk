<?php namespace Prudent\SPK\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Prudent\SPK\Models\Kriteria;
use Prudent\SPK\Models\Nilai as NilaiModel;
use Prudent\SPK\Models\BobotNilai;

/**
 * Nilai Back-end Controller
 */
class Nilai extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Prudent.SPK', 'spk', 'nilai');

    }

    public function listExtendColumns($list)
    {
        $kriteria = Kriteria::get();
        if(!empty($kriteria)){
            foreach($kriteria as $key => $value){
                $list->addColumns([
                    $value->id => [
                        'label' => $value->nama,
                        'type'  => 'nilai',
                        'sortable' => false
                    ],
                ]);        
            }
        }
        
    }


    public function update($recordId = null, $context = null)
    {
        try {

            $this->vars['kriteria'] = $kriteria = Kriteria::whereStatus(1)->get();
            $this->vars['bobot'] = $bobot = BobotNilai::get();

            $model = NilaiModel::find($recordId);
            $this->initForm($model);
        }
        catch (Exception $ex) {
            $this->controller->handleError($ex);
        }
    }

}
