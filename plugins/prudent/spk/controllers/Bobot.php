<?php namespace Prudent\SPK\Controllers;

use Request;
use Response;
use ApplicationException;
use BackendMenu;
use Backend\Classes\Controller;
use Prudent\SPK\Models\BobotItem;
use Flash;

/**
 * Bobot Back-end Controller
 */
class Bobot extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Prudent.SPK', 'spk', 'bobot');

    }


    public function update($recordId)
    {
        $this->addCss("/plugins/prudent/spk/assets/css/jquery.dataTables.min.css");
        $this->addJs("/plugins/prudent/spk/assets/js/jquery.dataTables.min.js");
        $this->addJs("/plugins/prudent/spk/assets/js/prudent.js");
        
        try {
             $this->vars['recordId'] = $recordId;
            $this->vars['bobot'] = $model = $this->formFindModelObject($recordId);
            $this->initForm($model);
        }
        catch (Exception $ex) {
            $this->controller->handleError($ex);
        }
    }


    public function update_onDeleteBobotItem($recordId)
    {
        $result['success'] = false;
        $result['data'] = null;

        try {
            $id = post('id');
            $item = BobotItem::find($id);
            $item->delete();
            
            $result['success'] = true;
            $result['data'] = $item;
        }
        catch (Exception $ex) {
            $result['success'] = false;
            $result['data'] = $ex;
        }
        return Response::json($result);
    }



    public function update_onGetBobotItem($recordId)
    {
        $result['success'] = false;
        $result['data'] = null;

        try {
            $id = post('id');

            $item = BobotItem::find($id);

            $result['success'] = true;
            $result['data'] = $item;
        }
        catch (Exception $ex) {
            $result['success'] = false;
            $result['data'] = $ex;
        }
        return Response::json($result);
    }


    public function update_onAddBobotItem($recordId)
    {
        $result['success'] = false;
        $result['data'] = null;

        try {
            $data = post('BobotItem');

            if(!empty($data)){
                $item = new BobotItem();
                $item->label = $data['label'];
                $item->nilai_min = $data['nilai_min'];
                $item->nilai_max = $data['nilai_max'];
                $item->bobot = $data['bobot'];
                $item->bobot_id = $recordId;
                $item->save();

            }
            $result['success'] = true;
            $result['data'] = $item;
        }
        catch (Exception $ex) {
            $result['success'] = false;
            $result['data'] = $ex;
        }
        return Response::json($result);
    }

}
