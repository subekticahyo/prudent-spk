<?php namespace Prudent\SPK\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Bobot Item Back-end Controller
 */
class BobotItem extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Prudent.SPK', 'spk', 'bobotitem');
    }
}
