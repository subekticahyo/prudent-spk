<?php namespace Prudent\SPK\Models;

use Model;

/**
 * Kriteria Model
 */
class Kriteria extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'prudent_spk_kriterias';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [
        'threshold' => ['Prudent\SPK\Models\BobotItem','key'=>'nilai_ideal','otherKey'=>'id']
    ];
    public $hasMany = [
       'nilai' => 'Prudent\SPK\Models\Nilai',
       'subkriteria' => ['Prudent\SPK\Models\Subkriteria','key'=>'id','otherKey'=>'kriteria_id'],
    ];
    public $belongsTo = [
       'bobot' => 'Prudent\SPK\Models\Bobot'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function getSubkriteria(){
        return $this;
    }  
}
