<?php namespace Prudent\SPK\Models;

use Model;

/**
 * Bobot Model
 */
class Bobot extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'prudent_spk_bobots';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [
       //'kriteria' => 'Prudent\SPK\Models\Kriteria'
    ];
    public $hasMany = [
        'detail' => 'Prudent\SPK\Models\BobotItem'
    ];
    public $belongsTo = [
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
