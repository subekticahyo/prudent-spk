<?php namespace Prudent\SPK\Models;

use Model;

/**
 * Kelas Model
 */
class Kelas extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'prudent_spk_kelas';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public $hasManyThrough = [
        'kriteria' => [
            'Prudent\SPK\Models\Kriteria',
            'through' => 'Prudent\SPK\Models\KelasKriteria',
            'key'        => 'id',
            'throughKey' => 'kelas_id'
        ]
    ];

}
