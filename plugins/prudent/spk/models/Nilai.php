<?php namespace Prudent\SPK\Models;

use Model;
use Db;
/**
 * Nilai Model
 */
class Nilai extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'prudent_spk_nilais';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [
        'bobotitem' => ['Prudent\SPK\Models\BobotItem','key'=>'id','otherKey'=>'nilai']
    ];
    public $hasMany = [
        'bobotitems' => ['Prudent\SPK\Models\BobotItem','key'=>'id','otherKey'=>'nilai']
    
    ];
    public $belongsTo = [
        'kriteria' => ['Prudent\SPK\Models\Kriteria','key'=>'kriteria_id','otherKey'=>'id'],
        'siswa' => ['Prudent\SPK\Models\Siswa','key'=>'siswa_id','otherKey'=>'id'],
    ];

    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];


    public static function getMin($kriteria_id){
        $return = Db::select('SELECT min(b.bobot) as minimum FROM prudent_spk_nilais n 
                join prudent_spk_bobot_items b on b.id = n.nilai
                join prudent_spk_bobots c on c.id = b.bobot_id
                join prudent_spk_kriterias d on d.bobot_id = c.id
                where n.kriteria_id = ?', [$kriteria_id]);
        return $return;
    }



    public static function getNormalisasiSAW($kriteria, $siswa){
        $result = 0;
        if(!empty($kriteria)){

            if($kriteria->kategori=='cost')
            {
                $item = new BobotItem();
                $nilai = new Nilai();
                $nilais = $nilai::join($item->table,$item->table.'.id','=',$nilai->table.'.nilai')
                    ->where($nilai->table.'.kriteria_id',$kriteria->id)
                    ->where($nilai->table.'.kriteria_id',$kriteria->id);

                $nilai_min_kategori = $nilais->min($nilai->table.'.nilai');
                $nilai_awal = $nilais->where('siswa_id',$siswa->id)->first();
                $result = (double)$nilai_min_kategori/$nilai_awal->bobot;
            }elseif($kriteria->kategori=='benefit'){

                $item = new BobotItem();
                $nilai = new Nilai();
                $nilais = $nilai::join($item->table,$item->table.'.id','=',$nilai->table.'.nilai')
                    ->where($nilai->table.'.kriteria_id',$kriteria->id)
                    ->where($nilai->table.'.kriteria_id',$kriteria->id);

                $nilai_max_kategori = $nilais->max($nilai->table.'.nilai');
                $nilai_awal = $nilais->where('siswa_id',$siswa->id)->first();
                $result = (double)$nilai_awal->bobot/$nilai_max_kategori;
            }else{
                $result = 0;
            }
        }
        return round($result,2);
    }

    public static function getNormalisasiPMGAP($kriteria, $siswa){
        $result = 0;
        if(!empty($kriteria)){

            if($kriteria->jenis_faktor=='core')
            {
                $item = new BobotItem();
                $nilai = new Nilai();
                $nilais = $nilai::join($item->table,$item->table.'.id','=',$nilai->table.'.nilai')
                    ->where($nilai->table.'.kriteria_id',$kriteria->id)
                    ->where($nilai->table.'.kriteria_id',$kriteria->id);
                $nilai_awal = $nilais->where('siswa_id',$siswa->id)->first();
                
                //PERHITUNGAN GAP
                $gap = $nilai_awal->bobot - $kriteria->nilai_ideal; 
                $result = $gap;

                /*$nilai_min_kategori = $nilais->min($nilai->table.'.nilai');
                $result = (double)$nilai_min_kategori/$nilai_awal->bobot;*/
            }elseif($kriteria->jenis_faktor=='secondary'){

                $item = new BobotItem();
                $nilai = new Nilai();
                $nilais = $nilai::join($item->table,$item->table.'.id','=',$nilai->table.'.nilai')
                    ->where($nilai->table.'.kriteria_id',$kriteria->id)
                    ->where($nilai->table.'.kriteria_id',$kriteria->id);
                $nilai_awal = $nilais->where('siswa_id',$siswa->id)->first();

                //PERHITUNGAN GAP
                $gap = $nilai_awal->bobot - $kriteria->nilai_ideal; 
                $result = $gap;

                /*$nilai_max_kategori = $nilais->max($nilai->table.'.nilai');
                $nilai_awal = $nilais->where('siswa_id',$siswa->id)->first();
                $result = (double)$nilai_max_kategori/$nilai_awal->bobot;*/
            }else{
                $result = 0;
            }
        }
        return round($result,2);
    }


    public static function getNormalisasiPM($kriteria, $siswa){
        $gap = Nilai::getNormalisasiPMGAP($kriteria, $siswa);
        $result = 0;
        $bobotid = Settings::get('pm_bobot_id');
        $bobotgap = Bobot::find($bobotid);
        if(!empty($bobotgap)){
            $temp = $bobotgap->detail->where('nilai_max',$gap)->first();
            if(!empty($temp)){
                $result = $temp->bobot;   
            }
        }else{
            $result = 0;
        }
        return round($result,2);
    }
}
