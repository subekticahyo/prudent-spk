<?php namespace Prudent\SPK\Models;

use Model;
use Prudent\SPK\Models\Nilai;
use Prudent\SPK\Models\Kriteria;

/**
 * Siswa Model
 */
class Siswa extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'prudent_spk_siswas';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [  
        ];
    public $hasMany = [
        'nilai' => [
            'Prudent\SPK\Models\Nilai',
            'key' => 'id',
            'otherKey' => 'siswa_id'
        ]
    ];

    public $hasManyThrough = [
        /*'kelas' => [
            'Prudent\SPK\Models\Kelas',
            'through' => 'Prudent\SPK\Models\TahunAjaran',
            'key'        => 'id',
            'throughKey' => 'kelas_id'
        ]*/
    ];



    public $belongsTo = [
        
          'kelas' => 'Prudent\SPK\Models\Kelas'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getNilaiCount(){
        return Nilai::whereSiswaId($this->id)->count();
    }

    public function getNilai($kriteria){
        $result =  Nilai::whereSiswaId($this->id)->whereKriteriaId($kriteria->id)->count();
    }

    public function getTotalKriteriaCount(){
        return Kriteria::count();
    }
}
