<?php namespace Prudent\SPK\Models;

use Model;

/**
 * Subkriteria Model
 */
class Subkriteria extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'prudent_spk_subkriterias';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [
    ];
    public $hasMany = [];
    public $belongsTo = [
         'kriteria' => ['Prudent\SPK\Models\Kriteria','key'=>'kriteria_id','otherKey'=>'id'],
         'threshold' => ['Prudent\SPK\Models\BobotItem','key'=>'nilai_ideal','otherKey'=>'id'],
         'unit' => ['Prudent\SPK\Models\Bobot','key'=>'bobot_id','otherKey'=>'id'],

    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
