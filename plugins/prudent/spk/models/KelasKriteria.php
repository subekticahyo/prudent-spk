<?php namespace Prudent\SPK\Models;

use Model;

/**
 * KelasKriteria Model
 */
class KelasKriteria extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'prudent_spk_kelas_kriterias';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
