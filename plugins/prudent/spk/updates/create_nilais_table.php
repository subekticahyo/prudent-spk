<?php namespace Prudent\SPK\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateNilaisTable extends Migration
{
    public function up()
    {
        Schema::create('prudent_spk_nilais', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('siswa_id');
            $table->integer('kriteria_id');
            $table->integer('nilai');

            $table->unique(['siswa_id','kriteria_id','nilai']);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('prudent_spk_nilais');
    }
}
