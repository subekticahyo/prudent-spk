<?php namespace Prudent\SPK\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateKelasTable extends Migration
{
    public function up()
    {
        Schema::create('prudent_spk_kelas', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('nama')->nullable();
            $table->string('jurusan')->nullable();
            $table->integer('tingkat')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('prudent_spk_kelas');
    }
}
