<?php namespace Prudent\SPK\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSiswasTable extends Migration
{
    public function up()
    {
        Schema::create('prudent_spk_siswas', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('nama')->nullable();
            $table->string('nis')->index();
            $table->string('nisn')->index();
            $table->string('jenis_kelamin',1)->nullable();

            //$table->string('jurusan')->nullable();
            //$table->string('kelas')->nullable();

            $table->integer('kelas_id')->nullable();
            
            $table->integer('status')->nullable();
            $table->string('create_by')->nullable();
            $table->string('update_by')->nullable();


            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('prudent_spk_siswas');
    }
}
