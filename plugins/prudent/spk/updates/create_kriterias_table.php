<?php namespace Prudent\SPK\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateKriteriasTable extends Migration
{
    public function up()
    {
        Schema::create('prudent_spk_kriterias', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('kode')->nullable();
            $table->string('nama')->nullable();
            $table->string('deskripsi')->nullable();
            $table->string('kategori')->default(0);
            $table->integer('nilai_kriteria')->nullable();
            $table->integer('bobot_id')->default(0);
            $table->integer('nilai_ideal')->nullable();
            $table->string('jenis_faktor')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('prudent_spk_kriterias');
    }
}
