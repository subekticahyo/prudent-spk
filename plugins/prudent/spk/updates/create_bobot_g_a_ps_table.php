<?php namespace Prudent\SPK\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateBobotGAPsTable extends Migration
{
    public function up()
    {
        Schema::create('prudent_spk_bobot_g_a_ps', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('prudent_spk_bobot_g_a_ps');
    }
}
