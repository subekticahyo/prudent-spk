<?php namespace Prudent\SPK\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateNilaiCrispsTable extends Migration
{
    public function up()
    {
        Schema::create('prudent_spk_nilai_crisps', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('kode')->index(0);
            $table->integer('kriteria_id')->index(0);
            $table->string('nama')->nullable();
            $table->string('deskripsi')->nullable();
            $table->integer('nilai_min')->default(0);
            $table->integer('nilai_max')->default(0);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('prudent_spk_nilai_crisps');
    }
}
