<?php namespace Prudent\SPK\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateBobotsTable extends Migration
{
    public function up()
    {
        Schema::create('prudent_spk_bobots', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('kode')->nullable();
            $table->string('nama')->nullable();
            $table->string('deskripsi')->nullable();
            
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('prudent_spk_bobots');
    }
}
