<?php namespace Prudent\SPK\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateBobotItemsTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('prudent_spk_bobot_items')) {
            Schema::create('prudent_spk_bobot_items', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');

                $table->string('label')->nullable();
                $table->integer('bobot_id')->default(0);
                $table->double('nilai_min')->default(0);
                $table->double('nilai_max')->default(0);
                $table->double('bobot')->default(0);

                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('prudent_spk_bobot_items');
    }
}
