<?php namespace Prudent\SPK\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateKelasKriteriasTable extends Migration
{
    public function up()
    {
        Schema::create('prudent_spk_kelas_kriterias', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('kelas_id')->nullable();
            $table->integer('kriteria_id')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('prudent_spk_kelas_kriterias');
    }
}
