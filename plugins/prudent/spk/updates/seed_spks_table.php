<?php namespace Acme\Users\Updates;

use Seeder;
use Prudent\SPK\Models\Siswa;
use Prudent\SPK\Models\Kriteria;

class SeedSpksTable extends Seeder
{
    public function run()
    {
        $siswa = Siswa::create([
            'nama'          => 'user@example.com',
            'nis'           => 'user',
            'nisn'          => 'password123',
            'jenis_kelamin' => 'password123',
        ]);

        Kriteria::create(['kode'=>'K1','nama'=>'Nilai Raport','rata-rata nilai','kategori'=>'benefit','bobot_id'=>15]);
        Kriteria::create(['kode'=>'K2','nama'=>'Ranking','ranking','kategori'=>'benefit','bobot_id'=>5 ]);
        Kriteria::create(['kode'=>'K3','nama'=>'Absensi','jumlah absensi','kategori'=>'cost','bobot_id'=>10]);
        Kriteria::create(['kode'=>'K4','nama'=>'Partisipasi','partisipasi','kategori'=>'benefit','bobot_id'=>10]);
        Kriteria::create(['kode'=>'K5','nama'=>'Extrakurikuler','ekstra','kategori'=>'benefit','bobot_id'=>10]);
        Kriteria::create(['kode'=>'K6','nama'=>'Kedisiplinan','kedisiplinan','kategori'=>'benefit','bobot_id'=>10]);
        Kriteria::create(['kode'=>'K7','nama'=>'Kepemimpinan','kepemimpinan','kategori'=>'benefit','bobot_id'=>10]);
        Kriteria::create(['kode'=>'K8','nama'=>'Akhlak',' akhlak','kategori'=>'benefit','bobot_id'=>10]);
        Kriteria::create(['kode'=>'K9','nama'=>'Pelanggaran','poin','kategori'=>'cost','bobot_id'=>10]);
    }
}